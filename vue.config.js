// Read version from package.json
const fs = require("fs")
const packageJson = fs.readFileSync("./package.json")
const version = JSON.parse(packageJson).version || 0

const webpack = require("webpack")
const CopyWebpackPlugin = require("copy-webpack-plugin");

// Stamp service worked with current version string!
const fileNameIn = "service-worker-source.js"
const fileNameOut = "service-worker-versioned.js"
fs.readFile(fileNameIn, "utf8", function(err, data) {
	if (err) {
		return console.log(err)
	}
	var result = data.replace(/APP_VERSION_PLACEHOLDER/g, "'" + version + "'")

	fs.writeFile(fileNameOut, result, "utf8", function(err) {
		if (err) return console.log(err)
	})
})

module.exports = {
	devServer: {
		headers: {
			"Access-Control-Allow-Origin": "*",
			"Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
			"Access-Control-Allow-Headers":
				"X-Requested-With, content-type, Authorization",
		},
		proxy: {
			"^/benarnews": {
				changeOrigin: true,
				cookieDomainRewrite: "localhost",
				//target: "https://rfa.org",
				target: "https://d3qxfofof42sr2.cloudfront.net",
				followRedirects: true,
				pathRewrite: {
					"^/benarnews": "",
				},
			},
			"^/get": {
				changeOrigin: true,
				cookieDomainRewrite: "localhost",
				target: "https://webget.rfa.org",
				followRedirects: true,
				pathRewrite: {
					"^/get": "",
				},
			},
		},
	},
	publicPath: process.env.NODE_ENV === "production" ? "./" : "./",

	pwa: {
		name: "BenarNews",
		themeColor: "#8E8E8E",
		appleMobileWebAppCapable: "yes",
		appleMobileWebAppStatusBarStyle: "white",
		manifestOptions: {
			start_url: "./index.html",
			background_color: "#FFFFFF",
			display: "standalone",
			iconPaths: {
				appleTouchIcon: "img/icons/apple-touch-icon-152x152.png",
				maskIcon: "img/icons/safari-pinned-tab.svg",
				msTileImage: "img/icons/msapplication-icon-144x144.png",
			},
			icons: [
				{
					src: "./img/icons/android-chrome-192x192.png",
					sizes: "192x192",
					type: "image/png",
				},
				{
					src: "./img/icons/android-chrome-512x512.png",
					sizes: "512x512",
					type: "image/png",
				},
				{
					src: "./img/icons/apple-touch-icon-60x60.png",
					sizes: "60x60",
					type: "image/png",
				},
				{
					src: "./img/icons/apple-touch-icon-76x76.png",
					sizes: "76x76",
					type: "image/png",
				},
				{
					src: "./img/icons/apple-touch-icon-120x120.png",
					sizes: "120x120",
					type: "image/png",
				},
				{
					src: "./img/icons/apple-touch-icon-152x152.png",
					sizes: "152x152",
					type: "image/png",
				},
				{
					src: "./img/icons/apple-touch-icon-180x180.png",
					sizes: "180x180",
					type: "image/png",
				},
				{
					src: "./img/icons/apple-touch-icon.png",
					sizes: "180x180",
					type: "image/png",
				},
				{
					src: "./img/icons/msapplication-icon-144x144.png",
					sizes: "144x144",
					type: "image/png",
				},
				{
					src: "./img/icons/mstile-150x150.png",
					sizes: "150x150",
					type: "image/png",
				},
			],
		},
		workboxPluginMode: "InjectManifest",
		workboxOptions: {
			swSrc: "service-worker-versioned.js",
			swDest: "service-worker.js",
			exclude: ["assets/fonts/", /\.map$/],
		},
	},

	configureWebpack: {
		devtool: "source-map",
		plugins: [
			new webpack.DefinePlugin({
				"process.env": {
					PACKAGE_VERSION: '"' + version + '"',
				},
			}),
			new CopyWebpackPlugin({
                patterns: [
                    {
                        from: "./src/proxies.js",
                        to: "./",
                        transform(content, absoluteFrom) {
                            const branch = process.env.CI_COMMIT_BRANCH || "development";
                            if (["master", "main"].includes(branch)) {
                                const input = content.toString();
                                return input.replace("const USE_PRODUCTION_PROXIES = false;", "const USE_PRODUCTION_PROXIES = true;");
                              }
                            return content;
                        }
                    }
                ]
            })

		],
	},

	lintOnSave: "warning",
}
