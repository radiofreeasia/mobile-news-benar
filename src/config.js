export default {
	english: {
		displayName: "English",
		name: "English",
		localeName: "en",
		dateFormat: "en",
		cssFile: "./assets/css/default.css",
		analytics: {
			enabled: false,
			type: "empty",
			config: {},
		},
		fonts: {
			LibreFranklin: {
				bolditalics: "LibreFranklin-BoldItalic.ttf",
				bold: "LibreFranklin-Bold.ttf",
				italics: "LibreFranklin-Italic.ttf",
				normal: "LibreFranklin-Regular.ttf",
			},
			NotoSerif: {
				bolditalics: "NotoSerif-BoldItalic.ttf",
				bold: "NotoSerif-Bold.ttf",
				italics: "NotoSerif-Italic.ttf",
				normal: "NotoSerif-Regular.ttf",
			},
		},
		services: [
			{
				title: "Main service",
				url: "/english/appcats/this-is-the-main-feed/mainfeed/app_main_rss.xml",
				categories: [
					{url: "/english/appcats/cat1/app_rss.xml"},
					{url: "/english/appcats/cat2/app_rss.xml"},
					{url: "/english/appcats/cat3/app_rss.xml"},
					{url: "/english/appcats/cat4/app_rss.xml"},
					{url: "/english/appcats/cat5/app_rss.xml"},
					{url: "/english/appcats/cat6/app_rss.xml"},
					{url: "/english/appcats/cat7/app_rss.xml"},
				],
			},
		],
		contactEmail: "inquiry@benarnews.org",
		contactPhone: "1 (202) 530-4900",
		contactWeb: "https://www.benarnews.org/english/about/contact.html",
	},
	bengali: {
		displayName: "Bengali",
		name: "বাংলা",
		localeName: "bn",
		dateFormat: "bn",
		cssFile: "./assets/css/bn.css",
		fonts: {
			NotoSansBengali: {
				bold: "NotoSansBengali-Bold.ttf",
				italics: "NotoSansBengali-Regular.ttf",
				normal: "NotoSansBengali-Regular.ttf",
			},
		},
		services: [
			{
				title: "Main service",
				url: "/bengali/appcats/main/app_main_rss.xml",
				categories: [
					{url: "/bengali/appcats/cat1/app_rss.xml"},
					{url: "/bengali/appcats/cat2/app_rss.xml"},
				],
			},
		],
		contactEmail: "inquiry@benarnews.org",
		contactPhone: "1 (202) 530-4900",
		contactWeb: "https://www.benarnews.org/bengali/about/contact.html",
	},
	indonesian: {
		displayName: "Bahasa Indonesia",
		name: "Bahasa Indonesia",
		localeName: "in_ID",
		dateFormat: "in",
		cssFile: "./assets/css/default.css",
        fonts: {
			LibreFranklin: {
				bolditalics: "LibreFranklin-BoldItalic.ttf",
				bold: "LibreFranklin-Bold.ttf",
				italics: "LibreFranklin-Italic.ttf",
				normal: "LibreFranklin-Regular.ttf",
			}
        },
		services: [
			{
				title: "Main service",
				url: "/indonesian/appcats/main/app_main_rss.xml",
				categories: [
					{url: "/indonesian/appcats/cat1/app_rss.xml"},
					{url: "/indonesian/appcats/cat2/app_rss.xml"},
					{url: "/indonesian/appcats/cat3/app_rss.xml"},
					{url: "/indonesian/appcats/cat4/app_rss.xml"},
					{url: "/indonesian/appcats/cat5/app_rss.xml"},
				],
			},
		],
		contactEmail: "inquiry@benarnews.org",
		contactPhone: "1 (202) 530-4900",
		contactWeb: "https://www.benarnews.org/indonesian/about/contact.html",
	},
	malay: {
		displayName: "Bahasa Malaysia",
		name: "Bahasa Malaysia",
		localeName: "ms",
		dateFormat: "ms",
		cssFile: "./assets/css/default.css",
        fonts: {
			LibreFranklin: {
				bolditalics: "LibreFranklin-BoldItalic.ttf",
				bold: "LibreFranklin-Bold.ttf",
				italics: "LibreFranklin-Italic.ttf",
				normal: "LibreFranklin-Regular.ttf",
			}
        },
		services: [
			{
				title: "Main service",
				url: "/malay/appcats/main/app_main_rss.xml",
				categories: [
					{url: "/malay/appcats/cat1/app_rss.xml"},
					{url: "/malay/appcats/cat2/app_rss.xml"},
					{url: "/malay/appcats/cat3/app_rss.xml"},
					{url: "/malay/appcats/cat4/app_rss.xml"},
					{url: "/malay/appcats/cat5/app_rss.xml"},
				],
			},
		],
		contactEmail: "inquiry@benarnews.org",
		contactPhone: "1 (202) 530-4900",
		contactWeb: "https://www.benarnews.org/malay/about/contact.html",
	},
	thai: {
		displayName: "Thai",
		name: "ไทย",
		localeName: "th",
		dateFormat: "th",
		cssFile: "./assets/css/th.css",
		fonts: {
			NotoSerifThai: {
				bold: "NotoSerifThai-Bold.ttf",
				italics: "NotoSerifThai-Regular.ttf",
				normal: "NotoSerifThai-Regular.ttf",
			},
		},
		services: [
			{
				title: "Main service",
				url: "/thai/appcats/main/app_main_rss.xml",
				categories: [
					{url: "/thai/appcats/cat1/app_rss.xml"},
					{url: "/thai/appcats/cat2/app_rss.xml"},
					{url: "/thai/appcats/cat3/app_rss.xml"},
					{url: "/thai/appcats/cat4/app_rss.xml"},
					{url: "/thai/appcats/cat5/app_rss.xml"},
				],
			},
		],
		contactEmail: "inquiry@benarnews.org",
		contactPhone: "1 (202) 530-4900",
		contactWeb: "https://www.benarnews.org/thai/about/contact.html",
	},
}
