import Vue from 'vue'
import VueI18n from 'vue-i18n'

import en from './translations/en.json';
import bn from './translations/bn.json';
import th from './translations/th.json';
import ms from './translations/ms.json';
import in_ID from './translations/in.json';


Vue.use(VueI18n)

export default new VueI18n({
  locale: 'en',
  fallbackLocale: 'en',
  silentFallbackWarn: true,
  messages: {
    en: en,
    bn: bn,
    th: th,
    in: in_ID,
    ms: ms,
 
  }
})